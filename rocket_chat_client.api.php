<?php

/**
 * Change the list of the chat buddies the user can have. By default, users can
 * message any other registered user, but you might want to filter the list.
 *
 * @param array $users
 *   The list of chat buddies.
 */
function hook_rocket_chat_client_chat_buddies_alter(array &$users) {
  foreach ($users as $id => $user) {
    /** @var \Drupal\user\UserInterface $userEntity */
    $userEntity = $user['entity'];
    if ($userEntity->id() == 148) {
      unset($users[$id]);
    }
  }
}
