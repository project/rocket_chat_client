(function ($, Drupal) {

  let settings = drupalSettings.rocketchat;
  let get_messages_url = settings.get_messages_url;
  let send_message_url = settings.send_message_url;
  let last_message_timestamp = settings.last_message_timestamp;

  function isMobileDevice() {
    console.log($(window).width());
    return $(window).width() < 520;
  }

  function getMessages() {
    $.ajax({
      type: 'GET',
      dataType: 'json',
      url: get_messages_url,
      data: {
        oldest: last_message_timestamp
      }
    }).done(function(data) {
      if (data.last_message_timestamp < last_message_timestamp) {
        return;
      }
      if (data.messages.length) {
        last_message_timestamp = data.last_message_timestamp;
        let messagesContainer = $('#rocket-chat-messages');
        messagesContainer.append(data.markup);
        messagesContainer.scrollTop(messagesContainer.prop("scrollHeight"));
      }
    }).fail(function() {
      // @todo
      console.log('Failed retrieving messages');
    })
  }

  function sendMessage(e) {
    let messageBox = $('#rocket-chat-message-box');
    let sendButton = $('#rocket-chat-send-button');
    let text = messageBox.val().trim();
    if(!text.length) {
      return;
    }

    messageBox.val('');
    if (!isMobileDevice()) {
      sendButton.hide();
    }
    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: send_message_url,
      data: {
        message: text,
        last_message_timestamp: last_message_timestamp
      }
    }).done(function(data) {
      last_message_timestamp = data.last_message_timestamp;
      let messagesContainer = $('#rocket-chat-messages');
      messagesContainer.append(data.markup);
      messagesContainer.scrollTop(messagesContainer.prop("scrollHeight"));
    }).fail(function() {
      // @todo
      console.log('Failed sending message');
    });
  }

  Drupal.behaviors.rocketchat = {
    attach:
      function (context) {
        $('#rocket-chat', context).once('rocket_chat_client').each(function () {
          if (get_messages_url == null) {
            return;
          }
          getMessages();
          window.setInterval(getMessages, 5000);

          new PerfectScrollbar('#rocket-chat-list');
          new PerfectScrollbar('#rocket-chat-messages');

          let messageBox = $('#rocket-chat-message-box');
          let sendButton = $('#rocket-chat-send-button');

          if (!isMobileDevice()) {
            sendButton.hide();
          }
          messageBox.on('input', function(){
            if ($(this).val().length > 1) {
              sendButton.fadeIn();
            } else {
              if (!isMobileDevice()) {
                sendButton.hide();
              }
            }
          });

          sendButton.click(sendMessage);
          messageBox.keyup(function(e) {
            if(e.keyCode == 13) {
              // Enter key was pressed in the messages box.
              sendMessage(e);
            }
          });
        });
      }
  };

}(jQuery, Drupal));
