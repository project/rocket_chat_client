<?php

namespace Drupal\rocket_chat_client\Service;

use ATDev\RocketChat\Chat as RocketChat;
use ATDev\RocketChat\Messages\Collection as RocketChatMessagesCollection;
use ATDev\RocketChat\Users\User as RocketChatUser;
use ATDev\RocketChat\Ims\Im as RocketChatInstantMessage;
use ATDev\RocketChat\Messages\Message as RocketChatMessage;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\rocket_chat_client\Entity\RocketChatAccesToken;
use Drupal\user\UserInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class RocketChatService {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $accessTokenStorage;

  /**
   * @var string
   */
  protected $rocketChatServerUrl;

  /**
   * @var string
   */
  protected $rocketChatRegistrationSecret;

  /**
   * @var string
   */
  protected $rocketChatAdminUserId;

  /**
   * @var string
   */
  protected $rocketChatAdminAccessToken;

  /**
   * @var string
   */
  protected $rocketChatAdminUsername;

  /**
   * @var string
   */
  protected $rocketChatAdminPassword;

  /**
   * @var string
   */
  protected $rocketChatCurrentUsername;

  /**
   * @var \Drupal\rocket_chat_client\RocketChatAccessTokenInterface
   */
  protected $accessToken;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(RouteMatchInterface $routeMatch, EntityTypeManagerInterface $entityTypeManager, AccountInterface $currentUser) {
    $this->routeMatch = $routeMatch;
    $this->logger = $this->getLogger('rocket_chat_client');
    $this->entityTypeManager = $entityTypeManager;
    $this->userStorage = $this->entityTypeManager->getStorage('user');
    $this->accessTokenStorage = $this->entityTypeManager->getStorage('rocket_chat_access_token');
    $this->account = $this->userStorage->load($currentUser->id());

    $rocketChatSettings = Settings::get('rocketchat', []);
    $this->rocketChatCurrentUsername = $this->getUserRocketChatUserName($this->account);
    $this->accessToken = $this->getUserAccessToken();

    $noLoginCredentialsProvided = empty($rocketChatSettings['admin_access_token'])
      && empty($rocketChatSettings['admin_password']);

    if (empty($rocketChatSettings['server'])
      || empty($rocketChatSettings['registration_secret'])
      || $noLoginCredentialsProvided
    ) {
      $this->logger->error('Rocket.chat configuration is missing.');
    }
    else {
      $this->rocketChatServerUrl = $rocketChatSettings['server'];
      $this->rocketChatRegistrationSecret = $rocketChatSettings['registration_secret'];
      if (!empty($rocketChatSettings['admin_user_id'])) {
        $this->rocketChatAdminUserId = $rocketChatSettings['admin_user_id'];
      }
      if (!empty($rocketChatSettings['admin_access_token'])) {
        $this->rocketChatAdminAccessToken = $rocketChatSettings['admin_access_token'];
      }
      if (!empty($rocketChatSettings['admin_username'])) {
        $this->rocketChatAdminUsername = $rocketChatSettings['admin_username'];
      }
      if (!empty($rocketChatSettings['admin_password'])) {
        $this->rocketChatAdminPassword = $rocketChatSettings['admin_password'];
      }
      RocketChat::setUrl($this->rocketChatServerUrl);
    }
  }

  /**
   * Login the user to the Rocket.Chat server.
   *
   * @return bool
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function login($retryNumber = 1) {
    if ($retryNumber > 3) {
      // We don't want to retry anymore in order to avoid having an infinite loop
      // if the newly created tokens are invalid.
      return FALSE;
    }
    RocketChat::logout();
    if (!empty($this->accessToken) && !$this->accessToken->isRevoked()) {
      // Try the Rocket.Chat access token login.
      RocketChat::setAuthUserId($this->accessToken->getRocketChatUserId());
      RocketChat::setAuthToken($this->accessToken->getToken());
      if ($this->checkLogin($this->accessToken->getRocketChatUserId(), $this->accessToken->getToken())) {
        return TRUE;
      }
      else {
        $error = RocketChat::getError();
        $this->logger->error("Could not login to the Rocket.Chat server. Error: \"$error\".");
        $this->accessToken->revoke();
        $this->accessToken->save();
        // Retry the login after invalidating the previous token.
        return $this->login($retryNumber + 1);
      }
    }
    else {
      // We need to login the admin in order to generate an access token.
      if (!$this->adminLogin()) {
        return FALSE;
      }
      $user = new RocketChatUser($this->rocketChatCurrentUsername);
      if (!$user->info()) {
        // We need to register the user.
        $user = $this->registerCurrentUser();
        if (empty($user) || !$user->info()) {
          return FALSE;
        }
      }
      $token = $user->createToken();
      if (!empty($token) && !empty($token->userId) && !empty($token->authToken)) {
        $tokenEntity = RocketChatAccesToken::create([
          'user_id' => $this->account->id(),
          'rocket_chat_user_id' => $token->userId,
          'token' => $token->authToken,
        ]);
        $tokenEntity->save();
        if (!empty($tokenEntity->id())) {
          // Retry the login now that we have an access token.
          $this->accessToken = $tokenEntity;
          return $this->login($retryNumber + 1);
        }
      }
      else {
        $error = RocketChat::getError();
        $this->logger->error("Could not generate an access token for the Rocket.Chat server. Error: \"$error\".");
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Returns the instant message session.
   *
   * @param \Drupal\user\UserInterface $chatBuddy
   *
   * @return \ATDev\RocketChat\Ims\Im
   * @throws \Exception
   */
  protected function getInstantMessageSession(UserInterface $chatBuddy) {
    $instantMessage = new RocketChatInstantMessage();
    $instantMessage->setUsername($this->getUserRocketChatUserName($chatBuddy));
    if (!$instantMessage->create()) {
      $instantMessageError = $instantMessage->getError();
      if (!empty(strstr($instantMessageError, '[error-invalid-user]'))) {
        // Let's try to register the chat buddy in Rocket.Chat server.
        $rocketChatBuddy = $this->registerUser($chatBuddy);
      }
      if (!$instantMessage->create() || empty($rocketChatBuddy) || !$rocketChatBuddy->info()) {
        // We couldn't retrieve the messages or register the buddy.
        $error = "Could not retrieve instant messages. Error: \"{$instantMessageError}\".";
        $this->logger->error($error);
        throw new \Exception($error);
      }
    }
    return $instantMessage;
  }

  /**
   * Get chat messages from a conversation with a chat buddy.
   *
   * @param \Drupal\user\UserInterface $chatBuddy
   *
   * @return array
   * @throws \Exception
   */
  public function getInstantMessages(UserInterface $chatBuddy, $oldestTimeStamp = NULL, $count = null) {
    $instantMessage = $this->getInstantMessageSession($chatBuddy);
    $options = [
      'undreads' => true,
    ];
    if (!empty($oldestTimeStamp)) {
      $options['oldest'] = $oldestTimeStamp;
    }
    if (!empty($count)) {
      $options['count'] = $count;
    }
    /** @var \ATDev\RocketChat\Messages\Collection $history */
    $history = $instantMessage->history($options);
    if (empty($history) || !$history instanceof RocketChatMessagesCollection) {
      return [];
    }
    $chatMessages = [];
    foreach ($history as $message) {
      /** @var RocketChatMessage $message */
      $timestamp = strtotime($message->getTs());
      $date = DrupalDateTime::createFromTimestamp($timestamp);
      /** @var \ATDev\RocketChat\Messages\Message $message */
      $chatMessages[] = [
        'is_my_message' => $message->getUsername() == $this->rocketChatCurrentUsername,
        'username' => $message->getUsername(),
        'timestamp' => $timestamp,
        'date' => $date->format(DateFormat::load('date_time')->getPattern()),
        'time' => $date->format(DateFormat::load('time')->getPattern()),
        'message' => $message->getMsg(),
        'ts' => $message->getTs(),
      ];
    }
    usort($chatMessages, function ($a, $b) {
      return $a['timestamp'] <=> $b['timestamp'];
    });
    return $chatMessages;
  }

  /**
   * Send a chat message to a chat buddy.
   *
   * @param \Drupal\user\UserInterface $chatBuddy
   * @param string $message
   *
   * @return bool
   *  TRUE if the message was successfully sent, FALSE otherwise.
   * @throws \Exception
   */
  public function sendInstantMessages(UserInterface $chatBuddy, $text) {
    $instantMessage = $this->getInstantMessageSession($chatBuddy);
    $roomId = $instantMessage->getRoomId();
    $message = new RocketChatMessage();
    $message->setRoomId($roomId);
    $message->setText($text);
    if (!$message->postMessage()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Check whether the user was successfully logged in (using an access token).
   *
   * @return bool
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function checkLogin($authUserId, $authToken) {
    $httpClient = new Client([
      "base_uri" => "{$this->rocketChatServerUrl}/api/v1/",
      "allow_redirects" => ["track_redirects" => true]
    ]);
    $options = [
      'timeout' => 60,
      'connect_timeout' => 60,
      'exceptions' => false,
      'headers' => [
        'X-User-Id' => $authUserId,
        'X-Auth-Token' => $authToken,
      ],
    ];
    try {
      $response = $httpClient->request('GET','me', $options);
      return ($response->getStatusCode() >= 200) && ($response->getStatusCode() < 300);
    }
    catch (GuzzleException $e) {
      $this->logger->error("Error when checking the login. Error: \"{$e->getMessage()}\".");
      return FALSE;
    }
  }

  /**
   * Login the administrator to the Rocket.Chat server.
   *
   * @return bool
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function adminLogin() {
    RocketChat::logout();
    if (!empty($this->rocketChatAdminUserId) && !empty($this->rocketChatAdminAccessToken)) {
      RocketChat::setAuthUserId($this->rocketChatAdminUserId);
      RocketChat::setAuthToken($this->rocketChatAdminAccessToken);
      if ($this->checkLogin($this->rocketChatAdminUserId, $this->rocketChatAdminAccessToken)) {
        return TRUE;
      }
    }
    if (!empty($this->rocketChatAdminUsername) && !empty($this->rocketChatAdminPassword)) {
      $login = RocketChat::login($this->rocketChatAdminUsername, $this->rocketChatAdminPassword);
      if ($login && RocketChat::getSuccess()) {
        return TRUE;
      }
    }
    $error = RocketChat::getError();
    $this->logger->error("Could not login the administrator to the Rocket.Chat server. Please check Rocket.Chat admin credentials. Error: \"{$error}\".");
    return FALSE;
  }

  /**
   * Register the current user to the Rocket.Chat server.
   *
   * @return \ATDev\RocketChat\Users\User|false
   */
  protected function registerCurrentUser() {
    return $this->registerUser($this->account);
  }

  /**
   * Register a user to the Rocket.Chat server.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return \ATDev\RocketChat\Users\User|false
   */
  protected function registerUser(UserInterface $user) {
    $rocketUser = new RocketChatUser();
    $rocketUser->setUserId($user->id());
    $rocketUser->setName($user->getDisplayName());
    $rocketUser->setUsername($this->getUserRocketChatUserName($user));
    $rocketUser->setEmail($user->getEmail());
    $rocketUser->setPassword($this->generateRandomPassword());
    $rocketUser->setSendWelcomeEmail(FALSE);
    $rocketUser->setVerified(TRUE);
    $rocketUser->register($this->rocketChatRegistrationSecret);
    if (!$rocketUser->info()) {
      $error = RocketChat::getError();
      $this->logger->error("Could not register the Rocket.Chat user. Error: \"{$error}\".");
      return FALSE;
    }
    return $rocketUser;
  }

  /**
   * Generate a random password.
   *
   * @param $length
   *
   * @return string
   */
  protected function generateRandomPassword($length = 24) {
    $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $repeat = str_repeat($x, ceil($length/strlen($x)));
    return substr(str_shuffle($repeat),1, $length);
  }

  /**
   * Clean up the username in order to be a valid Rocket.Chat username.
   *
   * @param string $username
   *
   * @return string
   */
  protected function cleanUpUsername($username) {
    return preg_replace('/[^A-Za-z0-9\-]/', '_', $username);
  }

  /**
   * Returns the Rocket.Chat username based on user's Drupal username.
   *
   * @param \Drupal\user\UserInterface $user
   *
   * @return string
   */
  protected function getUserRocketChatUserName(UserInterface $user) {
    return $this->cleanUpUsername($user->getAccountName());
  }

  /**
   * Get user access token.
   *
   * @return \Drupal\rocket_chat_client\RocketChatAccessTokenInterface|NULL
   */
  protected function getUserAccessToken() {
    /** @var \Drupal\rocket_chat_client\RocketChatAccessTokenInterface[] $tokens */
    $tokens = $this->accessTokenStorage->loadByProperties([
      'user_id' => $this->account->id(),
      'status' => TRUE,
    ]);
    return !empty($tokens) ? reset($tokens) : NULL;
  }

}
