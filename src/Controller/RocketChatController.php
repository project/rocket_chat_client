<?php

namespace Drupal\rocket_chat_client\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\rocket_chat_client\Form\ChatSendForm;
use Drupal\rocket_chat_client\Service\RocketChatService;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class RocketChatController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * @var \Drupal\rocket_chat_client\Service\RocketChatService
   */
  protected $rocketChatService;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  /**
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $cacheKillSwitch;

  /**
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   * @param \Drupal\rocket_chat_client\Service\RocketChatService $rocketChatService
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $cacheKillSwitch
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(RouteMatchInterface $routeMatch, RocketChatService $rocketChatService, KillSwitch $cacheKillSwitch) {
    $this->routeMatch = $routeMatch;
    $this->logger = $this->getLogger('rocket_chat_client');
    $this->userStorage = $this->entityTypeManager()->getStorage('user');
    $this->rocketChatService = $rocketChatService;
    $this->account = $this->userStorage->load($this->currentUser()->id());
    $this->cacheKillSwitch = $cacheKillSwitch;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('rocket_chat_client.rocketchat'),
      $container->get('page_cache_kill_switch')
    );
  }

  /**
   * Retrieves the chat users list.
   *
   * @return array
   */
  protected function getChatBuddies() {
    $users = [];
    /** @var \Drupal\user\UserInterface[] $userEntities */
    $userEntities = $this->userStorage->loadByProperties([
      'status' => TRUE,
    ]);
    foreach ($userEntities as $userEntity) {
      $userId = $userEntity->id();
      if (empty($userId) || $userId == $this->account->id()) {
        continue;
      }
      $users[$userId] = [
        'id' => $userId,
        'entity' => $userEntity,
        'name' => $userEntity->getDisplayName(),
        'chat_url' => Url::fromRoute('rocket_chat_client.direct_chat', ['chatBuddy' => $userId])->toString(),
        'is_current_chat' => FALSE,
      ];

      $messages = $this->rocketChatService->getInstantMessages($userEntity, NULL, 1);
      if (!empty($messages)) {
        $lastMessage = end($messages);
        $users[$userId]['last_message'] = $lastMessage;
      }
    }
    // Alter the list of chat buddies.
    $this->moduleHandler()->alter('rocket_chat_client_chat_buddies', $users);
    return $users;
  }

  /**
   * Rocketchat page.
   *
   * @param \Drupal\user\UserInterface|NULL $chatBuddy
   *
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function chatPage(UserInterface $chatBuddy = NULL) {
    $this->cacheKillSwitch->trigger();
    $build = [
      'chat' => [
        '#theme' => 'chat',
        '#user_name' => $this->account->getDisplayName(),
        '#users' => [],
        '#messages' => [],
      ],
      '#attached' => [
        'library' => [
          'rocket_chat_client/rocketchat',
        ],
        'drupalSettings' => [
          'rocketchat' => [
            'get_messages_url' => NULL,
            'send_message_url' => NULL,
            'last_message_timestamp' => NULL,
          ],
        ],
      ],
      '#cache' => ['max-age' => 0],
    ];
    $login = $this->rocketChatService->login();
    if (!$login) {
      $this->messenger()->addError(t('Could not login to the Rocket.Chat server. Please contact an administrator.'));
      return $build;
    }
    $chatUsers = $this->getChatBuddies();
    if (!empty($chatBuddy) && !empty($chatUsers[$chatBuddy->id()])) {
      $chatUsers[$chatBuddy->id()]['is_current_chat'] = TRUE;
      $build['chat']['#current_buddy_name'] = $chatBuddy->toLink();

      $getMessagesUrl = Url::fromRoute('rocket_chat_client.direct_chat.get_direct_messages', ['chatBuddy' => $chatBuddy->id()]);
      $build['#attached']['drupalSettings']['rocketchat']['get_messages_url'] = $getMessagesUrl->toString();
      $sendMessageUrl = Url::fromRoute('rocket_chat_client.direct_chat.send_direct_message', ['chatBuddy' => $chatBuddy->id()]);
      $build['#attached']['drupalSettings']['rocketchat']['send_message_url'] = $sendMessageUrl->toString();
    }
    elseif (!empty($chatBuddy) && !array_key_exists($chatBuddy->id(), $chatUsers)) {
      throw new AccessDeniedHttpException();
    }

    $build['chat']['#users'] = $chatUsers;
    return $build;
  }


  public function getDirectMessages(UserInterface $chatBuddy, Request $request) {
    $oldestTimeStamp = $request->query->get('oldest');
    $this->rocketChatService->login();
    $messages = $this->rocketChatService->getInstantMessages($chatBuddy, $oldestTimeStamp);
    $build = [
      '#theme' => 'chat_messages',
      '#messages' => $messages,
    ];
    $data = [
      'messages' => $messages,
      'markup' => \Drupal::service('renderer')->render($build),
    ];
    $lastMessage = end($messages);
    if (!empty($lastMessage)) {
      $data['last_message_timestamp'] = $lastMessage['ts'];
    }
    return new JsonResponse($data);
  }

  public function sendDirectMessage(UserInterface $chatBuddy, Request $request) {
    $data = [];
    $text = trim($request->request->get('message') ?: '');
    if (!empty($text)) {
      $this->rocketChatService->login();
      if (!$this->rocketChatService->sendInstantMessages($chatBuddy, $text)) {
        return new JsonResponse('The message could not be sent.', 400);
      }
      $oldestTimeStamp = $request->request->get('last_message_timestamp');
      $messages = $this->rocketChatService->getInstantMessages($chatBuddy, $oldestTimeStamp);
      $build = [
        '#theme' => 'chat_messages',
        '#messages' => $messages,
      ];
      $data = [
        'messages' => $messages,
        'markup' => \Drupal::service('renderer')->render($build),
      ];
      $lastMessage = end($messages);
      if (!empty($lastMessage)) {
        $data['last_message_timestamp'] = $lastMessage['ts'];
      }
    }
    return new JsonResponse($data);
  }

}
