<?php

namespace Drupal\rocket_chat_client;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Contact entity.
 * @ingroup content_entity_example
 */
interface RocketChatAccessTokenInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Revoke a token.
   */
  public function revoke();

  /**
   * Check if the token was revoked.
   *
   * @return bool
   *   TRUE if the token is revoked. FALSE otherwise.
   */
  public function isRevoked();

  /**
   * Returns the Rocket.Chat user id.
   *
   * @return string
   */
  public function getRocketChatUserId();

  /**
   * Returns the token value.
   *
   * @return string
   */
  public function getToken();

}
